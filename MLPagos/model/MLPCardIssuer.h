//
//  MLPCardIssuer.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLPCardIssuer : NSObject
@property (nonatomic, copy, readonly) NSString *identifier;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, readonly) NSURL *thumbnailURL;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
