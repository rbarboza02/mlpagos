//
//  BlockDefinition.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#ifndef BlockDefinition_h
#define BlockDefinition_h

#import <Foundation/Foundation.h>

typedef void (^SuccessArrayBlock)(NSArray *items);
typedef void (^FailureBlock)(NSError *error);


#endif /* BlockDefinition_h */
