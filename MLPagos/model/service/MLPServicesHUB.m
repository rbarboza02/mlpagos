//
//  MLPServicesHUB.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPServicesHUB.h"
#import "MLPPaymentService.h"

@interface MLPServicesHUB ()
@property (nonatomic) NSObject<MLPPaymentServiceProtocol> *paymentService;

@end

@implementation MLPServicesHUB

+ (id)sharedInstance {
    static MLPServicesHUB *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[self alloc] init];
    });
    return service;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _paymentService = [[MLPPaymentService alloc] init];
    }
    
    return self;
}

@end
