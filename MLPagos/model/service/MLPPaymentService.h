//
//  MLPPaymentService.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPRestBaseService.h"
#import "MLPPaymentServiceProtocol.h"

@interface MLPPaymentService : MLPRestBaseService <MLPPaymentServiceProtocol>

@end
