//
//  MLPPaymentService.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentService.h"
#import "MLPPaymentMethod.h"
#import "MLPCardIssuer.h"
#import "MLPInstallment.h"

@implementation MLPPaymentService

-(void)retrievePaymentMethodSuccess:(SuccessArrayBlock)success failure:(FailureBlock)failure {
    NSArray *items = @[@{ @"id" : @"1",
                          @"name" : @"Visa",
                          @"thumbnail" : @"",
                          @"status" : @"active"},
                       @{ @"id" : @"2",
                          @"name" : @"AMEX",
                          @"thumbnail" : @"",
                          @"status" : @"noactive"},
                       @{ @"id" : @"3",
                          @"name" : @"Mastercard",
                          @"thumbnail" : @"",
                          @"status" : @"active"}];
    
    NSMutableArray *payments = [NSMutableArray array];
    for (NSDictionary *dic in items) {
        MLPPaymentMethod *pm = [[MLPPaymentMethod alloc] initWithDictionary:dic];
        if ([pm active]) {
            [payments addObject:pm];
        }
    }
    
    success(payments);
}

- (void)retrieveCardIssuersWithContext:(id<MLPCardIssuerContextProtocol>)context success:(SuccessArrayBlock)success failure:(FailureBlock)failure {
    NSArray *items = @[@{ @"id" : @"1",
                          @"name" : @"Tarjeta Shopping",
                          @"thumbnail" : @"",
                          @"secure_thumbnail" : @""},
                       @{ @"id" : @"2",
                          @"name" : @"Banco Galicia",
                          @"thumbnail" : @"",
                          @"secure_thumbnail" : @""},
                       @{ @"id" : @"3",
                          @"name" : @"Citi",
                          @"thumbnail" : @"",
                          @"secure_thumbnail" : @""},
                       @{ @"id" : @"4",
                          @"name" : @"Nuevo Banco de Santa Fe",
                          @"thumbnail" : @"",
                          @"secure_thumbnail" : @""}];
    
    NSMutableArray *cardItems = [NSMutableArray array];
    for (NSDictionary *dic in items) {
        [cardItems addObject:[[MLPCardIssuer alloc] initWithDictionary:dic]];
    }
    
    success(cardItems);
    
}

- (void)retrieveInstallmentsWithContext:(id<MLPInstallmentContextProtocol>)context success:(SuccessArrayBlock)success failure:(FailureBlock)failure {
    NSArray *items = @[@{ @"installments" : @"1",
                          @"recomended_message" : @"1 coutas de $ 15,00 ($ 15,00)" },
                       @{ @"installments" : @"3",
                          @"recomended_message" : @"3 coutas de $ 5,00 ($ 15,00)" },
                       @{ @"installments" : @"6",
                          @"recomended_message" : @"6 coutas de $ 3,29 ($ 19,79)" },
                       @{ @"installments" : @"5",
                          @"recomended_message" : @"9 coutas de $ 2,49 ($ 22,49)" },
                       @{ @"installments" : @"6",
                          @"recomended_message" : @"12 couta de $ 2,01 ($ 24,23)" }];
    
    NSMutableArray *installments = [NSMutableArray array];
    for (NSDictionary *dic in items) {
        [installments addObject:[[MLPInstallment alloc] initWithDictionary:dic]];
    }
    
    success(installments);
}

@end
