//
//  MLPPaymentServiceProtocol.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPCardIssuerContextProtocol.h"
#import "MLPInstallmentContextProtocol.h"

@protocol MLPPaymentServiceProtocol <NSObject>
- (void)retrievePaymentMethodSuccess:(SuccessArrayBlock)sucsess failure:(FailureBlock)failure;
- (void)retrieveCardIssuersWithContext:(id<MLPCardIssuerContextProtocol>) context success:(SuccessArrayBlock)success failure:(FailureBlock)failure;
- (void)retrieveInstallmentsWithContext:(id<MLPInstallmentContextProtocol>)context success:(SuccessArrayBlock)success failure:(FailureBlock)failure;

@end
