//
//  MLPCardIssuer.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPCardIssuer.h"

@implementation MLPCardIssuer

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _identifier = dictionary[@"id"];
        _name = dictionary[@"name"];
        _thumbnailURL = dictionary[@"thumbnail"];
    }
    
    return self;
}

@end
