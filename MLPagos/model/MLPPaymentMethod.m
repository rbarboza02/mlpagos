//
//  MLPPaymentMethod.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentMethod.h"

@implementation MLPPaymentMethod
@synthesize identifier = _identifier;
@synthesize name = _name;
@synthesize thumbnailURL = _thumbnailURL;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _identifier = dictionary[@"id"];
        _name = dictionary[@"name"];
        _thumbnailURL = dictionary[@"thumbnail"];
        _active = [dictionary[@"status"] isEqualToString:@"active"];
    }
    
    return self;
}

@end
