//
//  MLPPaymentContext.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPPaymentMethod.h"
#import "MLPCardIssuer.h"
#import "MLPInstallment.h"
#import "MLPPaymentAmountContextProtocol.h"
#import "MLPPaymentMethodContextProtocol.h"
#import "MLPCardIssuerContextProtocol.h"
#import "MLPInstallmentContextProtocol.h"
#import "MLPPaymentDetailContextProtocol.h"

@interface MLPPaymentContext : NSObject <MLPPaymentAmountContextProtocol, MLPPaymentMethodContextProtocol, MLPCardIssuerContextProtocol, MLPInstallmentContextProtocol, MLPPaymentDetailContextProtocol>

- (void)setPaymentAmount:(NSNumber *)paymentAmount;
- (void)setPaymentMethod:(MLPPaymentMethod *)paymentMethod;
- (void)setCardIssuer:(MLPCardIssuer *)cardIssuer;
- (void)setPaymentInstallment:(MLPInstallment *)installment;

- (NSNumber *)paymentAmount;
- (MLPPaymentMethod *)paymentMethod;
- (MLPCardIssuer *)cardIssuer;
- (MLPInstallment *)installment;

- (BOOL)isValidPayment;

@end
