//
//  MLPMangerCardIssuerProtocol.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MLPCardIssuer;
@class MLPPaymentMethod;
@protocol MLPCardIssuerContextProtocol <NSObject>
- (void)setCardIssuer:(MLPCardIssuer *)cardIssuer;
- (MLPCardIssuer *)cardIssuer;
- (MLPPaymentMethod *)paymentMethod;

@end
