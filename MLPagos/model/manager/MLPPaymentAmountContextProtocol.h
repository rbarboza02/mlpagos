//
//  MLPManagerPaymentAmountProtocol.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MLPPaymentAmountContextProtocol <NSObject>
- (void)setPaymentAmount:(NSNumber *)paymentAmount;
- (NSNumber *)paymentAmount;

@end
