//
//  MLPPaymentContext.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentContext.h"
#import "MLPPayment.h"

@interface MLPPaymentContext ()
@property (nonatomic) MLPPayment *payment;

@end

@implementation MLPPaymentContext

- (MLPPayment *)payment {
    if ( !_payment ) {
        _payment = [MLPPayment new];
    }
    
    return _payment;
}

- (void)setPaymentAmount:(NSNumber *)paymentAmount {
    self.payment.paymentAmount = paymentAmount;
    self.payment.paymentMethod = nil;
    self.payment.cardissuer = nil;
    self.payment.installment = nil;
}

- (void)setPaymentMethod:(MLPPaymentMethod *)paymentMethod {
    self.payment.paymentMethod = paymentMethod;
    self.payment.cardissuer = nil;
    self.payment.installment = nil;
}

- (void)setCardIssuer:(MLPCardIssuer *)cardIssuer {
    self.payment.cardissuer = cardIssuer;
    self.payment.installment = nil;
}

- (void)setPaymentInstallment:(MLPInstallment *)installment {
    self.payment.installment = installment;
}

- (NSNumber *)paymentAmount {
    return self.payment.paymentAmount;
}

- (MLPPaymentMethod *)paymentMethod {
    return self.payment.paymentMethod;
}

- (MLPCardIssuer *)cardIssuer {
    return self.payment.cardissuer;
}

- (MLPInstallment *)installment {
    return self.payment.installment;
}

- (BOOL)isValidPayment {
    return self.payment.paymentAmount && self.payment.paymentMethod && self.payment.cardissuer && self.payment.installment;
}

@end
