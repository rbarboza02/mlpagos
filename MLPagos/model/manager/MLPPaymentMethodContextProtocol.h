//
//  MLPManagerPaymentMethodProtocol.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MLPPaymentMethod;
@protocol MLPPaymentMethodContextProtocol <NSObject>
- (void)setPaymentMethod:(MLPPaymentMethod *)paymentMethod;
- (MLPPaymentMethod *)paymentMethod;

@end
