//
//  MLPPaymentDetailContextProtocol.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MLPPaymentMethod;
@class MLPCardIssuer;
@class MLPInstallment;
@protocol MLPPaymentDetailContextProtocol <NSObject>
- (NSNumber *)paymentAmount;
- (MLPPaymentMethod *)paymentMethod;
- (MLPCardIssuer *)cardIssuer;
- (MLPInstallment *)installment;
@end
