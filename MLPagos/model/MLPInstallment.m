//
//  MLPInstallment.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPInstallment.h"

@implementation MLPInstallment

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _identifier = dictionary[@"installments"];
        _name = dictionary[@"recomended_message"];
    }
    
    return self;
}

@end
