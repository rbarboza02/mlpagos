//
//  MLPPayment.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPPaymentMethod.h"
#import "MLPCardIssuer.h"
#import "MLPInstallment.h"

@interface MLPPayment : NSObject
@property (nonatomic) NSNumber *paymentAmount;
@property (nonatomic) MLPPaymentMethod *paymentMethod;
@property (nonatomic) MLPCardIssuer *cardissuer;
@property (nonatomic) MLPInstallment *installment;

@end
