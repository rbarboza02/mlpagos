//
//  MLPPaymentMethodViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentMethodViewController.h"
#import "MLPPaymentMethodTableViewCell.h"
#import "MLPServicesHUB.h"
#import "MLPCardIssuerViewController.h"
#import "UIViewController+MLP.h"

@interface MLPPaymentMethodViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *pItems;

@end

@implementation MLPPaymentMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[[MLPServicesHUB sharedInstance] paymentService] retrievePaymentMethodSuccess:^(NSArray *items) {
        _pItems = items;
    } failure:^(NSError *error) {
        //MOstrar cartel de error
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _pItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MLPPaymentMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pMethodTVCellIdentifier"];
    [cell updateCellWith:_pItems[indexPath.row]];
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    MLPCardIssuerViewController *vc = segue.destinationViewController;
    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    _manager.paymentMethod = _pItems[selectedIndexPath.row];
    vc.manager = (NSObject<MLPCardIssuerContextProtocol> *)_manager;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return [_tableView indexPathForSelectedRow] != nil;
}


#pragma mark - UIAction
- (IBAction)nextButtonTouched:(id)sender {
    if ( ![_tableView indexPathForSelectedRow] ) {
        [self displaySimpleAlertWithTitle:@"Pagos" message:@"Debe seleccionar algun Medio de Pago" callback:nil];
    }
}

@end
