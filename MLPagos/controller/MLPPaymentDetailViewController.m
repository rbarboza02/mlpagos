//
//  MLPPaymentDetailViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentDetailViewController.h"
#import "MLPCardIssuer.h"
#import "MLPPaymentMethod.h"
#import "MLPInstallment.h"

@interface MLPPaymentDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paymentMethodImageView;
@property (weak, nonatomic) IBOutlet UILabel *paymentMethodLabel;
@property (weak, nonatomic) IBOutlet UIImageView *banckImageView;
@property (weak, nonatomic) IBOutlet UILabel *bankLabel;
@property (weak, nonatomic) IBOutlet UILabel *intallmentLabel;

@end

@implementation MLPPaymentDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _amountLabel.text = _manager.paymentAmount.stringValue;
    _paymentMethodLabel.text = _manager.paymentMethod.name;
    _bankLabel.text = _manager.cardIssuer.name;
    _intallmentLabel.text = _manager.installment.name;
    
}

- (IBAction)payButtonTouched:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
