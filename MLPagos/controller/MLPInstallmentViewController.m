//
//  MLPInstallmentViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPInstallmentViewController.h"
#import "UIViewController+MLP.h"
#import "MLPServicesHUB.h"
#import "MLPInstallmentTableViewCell.h"
#import "MLPPaymentDetailViewController.h"
#import "MLPPaymentDetailContextProtocol.h"

@interface MLPInstallmentViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *items;

@end

@implementation MLPInstallmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addSpinner];
    [[[MLPServicesHUB sharedInstance] paymentService] retrieveInstallmentsWithContext:_manager success:^(NSArray *items) {
        [self removeSpinner];
        _items = items;
        [_tableView reloadData];
    } failure:^(NSError *error) {
        [self removeSpinner];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( ![_tableView indexPathForSelectedRow] ) {
        [self displaySimpleAlertWithTitle:@"Pagos" message:@"Debe seleccionar las Cuotas" callback:nil];
    }
    
    return [_tableView indexPathForSelectedRow] != nil;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    MLPPaymentDetailViewController *detailViewController = segue.destinationViewController;
    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    [_manager setPaymentInstallment:_items[selectedIndexPath.row]];
    detailViewController.manager = (NSObject<MLPPaymentDetailContextProtocol> *)_manager;
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MLPInstallmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"installmentCellIdentifier"];
    [cell updateWithInstallment:_items[indexPath.row]];
    
    return cell;
}

@end
