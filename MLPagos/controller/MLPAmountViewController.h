//
//  MLPAmountViewController.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPPaymentAmountContextProtocol.h"

@interface MLPAmountViewController : UIViewController
@property (nonatomic) NSObject<MLPPaymentAmountContextProtocol> *manager;

@end
