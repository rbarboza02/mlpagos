//
//  MLPCardIssuerViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPCardIssuerViewController.h"
#import "MLPServicesHUB.h"
#import "MLPInstallmentViewController.h"
#import "UIViewController+MLP.h"
#import "MLPCardIssuerTableViewCell.h"
#import "MLPInstallmentContextProtocol.h"

@interface MLPCardIssuerViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray *items;

@end

@implementation MLPCardIssuerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addSpinner];
    [[[MLPServicesHUB sharedInstance] paymentService] retrieveCardIssuersWithContext:_manager success:^(NSArray *items) {
        [self removeSpinner];
        _items = items;
        [_tableView reloadData];
    } failure:^(NSError *error) {
        [self removeSpinner];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ( ![_tableView indexPathForSelectedRow] ) {
        [self displaySimpleAlertWithTitle:@"Pagos" message:@"Debe seleciconar un Banco" callback:nil];
    }
    
    return [_tableView indexPathForSelectedRow] != nil;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    MLPInstallmentViewController *installmentViewController = segue.destinationViewController;
    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    _manager.cardIssuer = _items[selectedIndexPath.row];
    installmentViewController.manager = (NSObject<MLPInstallmentContextProtocol> *)_manager;
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MLPCardIssuerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cardiIssuerCellIdentifier"];
    [cell updateWithCardIssuer:_items[indexPath.row]];
    
    return cell;
}

@end
