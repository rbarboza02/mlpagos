//
//  MLPPaymentDetailViewController.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPPaymentDetailContextProtocol.h"

@interface MLPPaymentDetailViewController : UIViewController
@property (nonatomic) NSObject<MLPPaymentDetailContextProtocol> *manager;
@end
