//
//  MLPDebtsTableViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPDebtsTableViewController.h"
#import "MLPPaymentContext.h"
#import "MLPAmountViewController.h"

@interface MLPDebtsTableViewController ()
@property (nonatomic) MLPPaymentContext *paymentManager;

@end

@implementation MLPDebtsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _paymentManager = [MLPPaymentContext new];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MLPAmountViewController *amountViewController = segue.destinationViewController;
    amountViewController.manager = _paymentManager;
}

@end
