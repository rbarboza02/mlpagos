//
//  MLPAmountViewController.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPAmountViewController.h"
#import "UIViewController+MLP.h"
#import "MLPPaymentMethodViewController.h"
#import "MLPPaymentMethodContextProtocol.h"

@interface MLPAmountViewController ()
@property (weak, nonatomic) IBOutlet UITextField *amountTexField;

@end

@implementation MLPAmountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    MLPPaymentMethodViewController *vc = segue.destinationViewController;
    vc.manager = (NSObject<MLPPaymentMethodContextProtocol> *)_manager;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    if ([_amountTexField isFirstResponder]) {
        [_amountTexField resignFirstResponder];
    }
    
    
}
- (IBAction)nextButtonTouched:(id)sender {
    if (!_manager.paymentAmount) {
        [self displaySimpleAlertWithTitle:@"Pagos" message:@"Debe ingresar el monto a pagar" callback:nil];
    } else {
        [self performSegueWithIdentifier:@"paymentMethodSegueIdentifier" sender:nil];
    }
    
}

#pragma mark - UITextFieldDelegate
- (IBAction)textFieldEditingDidEnd:(id)sender {
    _manager.paymentAmount = @(_amountTexField.text.doubleValue);
}


@end
