//
//  UIViewController+MLP.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "UIViewController+MLP.h"

@implementation UIViewController (MLP)

- (void)addSpinner {
    
}

- (void)removeSpinner {
    
}

- (void)displaySimpleAlertWithTitle:(NSString *)title message:(NSString *)message callback:(void(^)(void))callbackBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert ];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                            if (callbackBlock) {
                                                                callbackBlock();
                                                            }
                                                        }];
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
