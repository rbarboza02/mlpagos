//
//  MLPInstallmentTableViewCell.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPInstallmentTableViewCell.h"

@interface MLPInstallmentTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation MLPInstallmentTableViewCell

- (void)updateWithInstallment:(MLPInstallment *)installment {
    _nameLabel.text = installment.name;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.backgroundColor = selected ? [UIColor yellowColor] : [UIColor whiteColor];
}
@end
