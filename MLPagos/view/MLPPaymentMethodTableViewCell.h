//
//  MLPPaymentMethodTableViewCell.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPPaymentMethod.h"

@interface MLPPaymentMethodTableViewCell : UITableViewCell
- (void)updateCellWith:(MLPPaymentMethod *)paymentMethod;

@end
