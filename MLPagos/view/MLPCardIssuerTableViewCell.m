//
//  MLPCardIssuerTableViewCell.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPCardIssuerTableViewCell.h"

@interface MLPCardIssuerTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *cImageView;
@property (weak, nonatomic) IBOutlet UILabel *cTitleLabel;

@end

@implementation MLPCardIssuerTableViewCell

- (void)updateWithCardIssuer:(MLPCardIssuer *)cardIssuer {
    //cargar imagen con el pod para cachear imagenes
    _cTitleLabel.text = cardIssuer.name;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.backgroundColor = selected ? [UIColor yellowColor] : [UIColor whiteColor];
}
@end
