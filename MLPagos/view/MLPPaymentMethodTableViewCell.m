//
//  MLPPaymentMethodTableViewCell.m
//  MLPagos
//
//  Created by Ruben D Barboza on 7/24/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import "MLPPaymentMethodTableViewCell.h"

@interface MLPPaymentMethodTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *cImageView;
@property (weak, nonatomic) IBOutlet UILabel *cTitleLabel;

@end

@implementation MLPPaymentMethodTableViewCell

- (void)updateCellWith:(MLPPaymentMethod *)paymentMethod {
    //cargar imagen con el pod para cachear imagenes
    _cTitleLabel.text = paymentMethod.name;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.backgroundColor = selected ? [UIColor yellowColor] : [UIColor whiteColor];
}

@end
