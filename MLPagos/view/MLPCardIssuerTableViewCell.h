//
//  MLPCardIssuerTableViewCell.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPCardIssuer.h"

@interface MLPCardIssuerTableViewCell : UITableViewCell
- (void)updateWithCardIssuer:(MLPCardIssuer *)cardIssuer;

@end
