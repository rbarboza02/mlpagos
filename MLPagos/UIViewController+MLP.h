//
//  UIViewController+MLP.h
//  MLPagos
//
//  Created by Ruben D Barboza on 7/25/16.
//  Copyright © 2016 Ruben D Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MLP)

//indicadores de carga
- (void)addSpinner;
- (void)removeSpinner;

//alerta
- (void)displaySimpleAlertWithTitle:(NSString *)title message:(NSString *)message callback:(void(^)(void))callbackBlock;
@end
